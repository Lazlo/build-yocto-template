/*
 * Jenkinsfile - Delarative Pipeline for Building Yocto Project Poky
 */

@Library('pipeline-library') _

pipeline {
    agent {
        node {
            label 'linux && debian && sudo'
            // Use a custom workspace without '@' and '%' character in the path.
            // BitBake QA scripts to not like funny characters in the path
            // to the build directory.
            customWorkspace "/var/lib/jenkins/.jenkins/workspace/${JOB_NAME.replace('%', '')}/build_${BUILD_NUMBER}"
        }
    }
    environment {
        APT_LOCK        = "apt-lock-${env.NODE_NAME}"
        CLEAN_CMD       = 'rm -rf poky'
    }
    stages {
        stage('Setup') {
            steps {
                echo 'Setting up ...'
                timeout(time: 10, unit: 'MINUTES') {
                    retry(5) {
                        lock("${env.NODE_NAME}") {
                            sh 'sudo ./scripts/setup.sh'
                        }
                    }
                }
            }
        }
        stage('Clean') {
            steps {
                sh '${CLEAN_CMD}'
            }
        }
        stage('Fetch') {
            steps {
                sh './scripts/fetch.sh'
            }
        }
        stage('Build') {
            environment {
                TMP_BASE_DIR    = '/var/build/yocto'
                DL_DIR          = '/var/build/yocto/downloads'
                SSTATE_DIR      = '/var/build/yocto/sstate-cache'
            }
            steps {
                echo 'Building ...'
                sh 'mkdir -p $TMP_BASE_DIR'
                sh './scripts/build.sh'
            }
            post {
                success {
                    archiveArtifacts artifacts: 'poky/build-beaglebone-yocto/tmp/deploy/images/beaglebone-yocto/*'
                }
            }
        }
    }
    post {
        always {
            commonStepNotification()
        }
    }
}
