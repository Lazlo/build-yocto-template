Yocto & BitBake Cheat Sheet
===========================

Lets start right after cloning the poky repo.

You will always want to create a build directory. This is done using the `oe-init-build-env` environment file.
When this file is _sourced_ it will create a build directory from a template and exit after changing into this directory.


Build Directory
---------------

In this example we will create a build directory for Qemu for `x86_64` architecture.

```
. oe-init-build-env build_qemux86-64
```

Now that the environment is loaded and we are in the build directory, some new commands become available.
Here is a list of the commands I tend to use frequently.

 - `bitbake`
 - `bitbake-layers`
 - `runqemu`


Build Configuration
-------------------

Lets review and adjust the configuration found in `conf/local.conf` before running a build.

As we are building for Qemu we do not need to modify the `MACHINE` variable. If we would
be building for another machine the the Beagle Bone we need to adjust this variable.

The following should always be set (but will become part of a future `local.conf.sample`):

```
DL_DIR = "/var/build/yocto/downloads"
SSTATE_DIR = "/var/build/yocto/sstate-cache"

PACKAGE_CLASSES = "package_deb"

INHERIT += "ccache"
CCACHE_DIR = "/var/build/ccache"

INHERIT += "buildhistory"
BUILDHISTORY_COMMIT = "1"
```

By setting `DL_DIR` and `SSTATE_DIR` we make sure that files downloaded or intermediate build results
will be saved outside the build directory (in our custom build cache location).

Setting `PACKAGE_CLASSES` results in the build producing Debian packages (which are used to populate
a disk image).

Next we enable ccache support in BitBake.

Lastly we enable the build history feature in BitBake. FIXME Explain why we need it and what it does.
See its documentation at https://www.yoctoproject.org/docs/1.8/ref-manual/ref-manual.html#maintaining-build-output-quality

FIXME Mention the values related to Qemu configuration found in `conf/local.conf`

Building
--------

 - FIXME Describe how to build a recipe
 - FIXME Describe where the find the build results
 - FIXME Describe how to list available images

