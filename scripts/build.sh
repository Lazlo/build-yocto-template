#!/bin/bash

: "${MACHINE:=beaglebone-yocto}"
: "${IMAGE:=core-image-sato-dev}"

cd poky

source oe-init-build-env build-$MACHINE

DL_DIR="${DL_DIR:-\$\{TOPDIR\}/../downloads}"
SSTATE_DIR="${SSTATE_DIR:-\$\{TOPDIR\}/../sstate-cache}"

# Escape paths for use with sed
DL_DIR_ESCAPED="$(echo $DL_DIR | sed 's/\//\\\//g')"
SSTATE_DIR_ESCAPED="$(echo $SSTATE_DIR | sed -E 's/\//\\\//g')"

sed -i -E "s/^#(DL_DIR) \?= .*/\1 ?= \"$DL_DIR_ESCAPED\"/" conf/local.conf
sed -i -E "s/^#(SSTATE_DIR) \?= .*/\1 ?= \"$SSTATE_DIR_ESCAPED\"/" conf/local.conf
sed -i -E 's/^(PACKAGE_CLASSES) \?= .*/\1 ?= "package_deb"/' conf/local.conf
sed -i -E "s/^(MACHINE) \?\?= .*/\1 ??= \"$MACHINE\"/" conf/local.conf
echo "# Enable ccache" >> conf/local.conf
echo 'INHERIT += "ccache"' >> conf/local.conf
echo "CCACHE_DIR = \"$(ccache --get-config=cache_dir)\"" >> conf/local.conf
echo '# Enable buildhistory' >> conf/local.conf
echo 'INHERIT += "buildhistory"' >> conf/local.conf
echo 'BUILDHISTORY_COMMIT = "1"' >> conf/local.conf

bitbake $IMAGE
