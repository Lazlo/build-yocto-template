#!/bin/bash

set -e
set -u

SFDISK_BIN=sfdisk
SFDISK_FLAGS="-q"
SFDISK="$SFDISK_BIN $SFDISK_FLAGS"

getpartname() {
	local -r disk_file="$1"
	local -r disk_partno="$2"
	# get the string between the last / and the last character before the line end
	# so for /dev/mmcblk0 get the "mmcblk" part
	# for /dev/sdc get the "sd" part
	local -r disk_type=$(echo $disk_file | sed -E 's/.*\/(.*)[0-9a-z]/\1/')

	case $disk_type in
	sd)
		echo "${disk_file}${disk_partno}"
		;;
	mmcblk)
		echo "${disk_file}p${disk_partno}"
		;;
	esac
}

clean_disk() {
	local -r disk_file="$1"
	wipefs --all $disk_file
	set +e
	sfdisk --delete $disk_file
	set -e
}

create_partitions() {
	local -r disk_file="$1"
	local -r part_boot_name="boot"
	local -r part_root_name="rootfs"
	local -r part_boot_size_mb=$2
	local -r part_root_size_mb=$3
	local -r part_boot_type=0x0b
	local -r part_root_type=0x83
	echo 'label: dos' | $SFDISK $disk_file
	sync && sleep 1
	echo "name=\"${part_boot_name}\" size=${part_boot_size_mb}MiB type=${part_boot_type} bootable" | $SFDISK --append $disk_file
	sync && sleep 1
	echo "name=\"${part_root_name}\" size=${part_root_size_mb}MiB type=${part_root_type}" | $SFDISK --append $disk_file
	sync && sleep 1
	echo ',+ ' | $SFDISK --append -N 2 --force $disk_file
}

create_filesystems() {
	local -r disk_file="$1"
	local -r part_boot_fs="$2"
	local -r part_root_fs="$3"
	if [[ "$part_boot_fs" == "vfat" ]]; then
		mkfs.vfat -n BOOT $(getpartname $disk_file 1)
	else
		>&2 echo "ERROR: Unknown file system type \"$part_boot_fs\"!"
		exit 1
	fi
	if [[ "$part_root_fs" == "ext4" ]]; then
		mkfs.ext4 -q -F -L ROOTFS $(getpartname $disk_file 2)
	else
		>&2 echo "ERROR: Unknown file system type \"$part_root_fs\"!"
	fi
}

setup_mnt() {
	local -r tmpdir="$1"
	mkdir -p $tmpdir/boot
	mkdir -p $tmpdir/root
}

clean_mnt() {
	local -r tmpdir="$1"
	rmdir $tmpdir/boot
	rmdir $tmpdir/root
	rmdir $tmpdir
}

deploy_boot() {
	local -r yocto_build_dir="$1"
	local -r machine="$2"
	local -r dest="$3"
	local -r src="$yocto_build_dir/tmp/deploy/images/$machine"
	cp $src/MLO $dest
	cp $src/u-boot.img $dest
}

deploy_root() {
	local -r yocto_build_dir="$1"
	local -r machine="$2"
	local -r dest="$3"
	local -r img_name="$4"
	src="$yocto_build_dir/tmp/deploy/images/$machine"
	# Copy DTB
	# Copy zImage
	# Deploy root file system
	tar xjf $src/${img_name}-${machine}.tar.bz2 -C $dest
	# Deploy kernel modules
	tar xzf $src/modules-${machine}.tgz -C $dest
}

main() {
	if [ $# != 4 ]; then
		echo "USAGE: $0 <disk-device-file> <yocto-build-dir> <image-name> <machine>"
		exit 1
	fi
	if [ $UID != 0 ]; then
		echo "ERROR: Run as root!"
		exit 1
	fi
	local -r disk_file="$1"
	local -r yocto_build_dir="$2"
	local -r img_name="$3"
	local -r machine="$4"
	local -r tmpdir="$(mktemp -d)"

	local -r part_boot_size_mb=40
	local -r part_boot_fs="vfat"
	local -r part_root_size_mb=1024
	local -r part_root_fs="ext4"

	# Make sure block device is not mounted
	set +e
	mount | grep $disk_file > /dev/null
	rc=$?
	set -e
	if [ "$rc" = "0" ]; then
		echo "WARNING: Detected target block device is mounted!"
		mnt_point_list="$(mount | grep sdc | sed -E 's/^.* on (.*) type .*$/\1/')"
		for mp in $mnt_point_list; do
			echo "Unmounting $mp"
			umount $mp
		done
	fi

	clean_disk $disk_file
	create_partitions $disk_file $part_boot_size_mb $part_root_size_mb
	create_filesystems $disk_file $part_boot_fs $part_root_fs
	setup_mnt $tmpdir
	# mount
	mount $(getpartname $disk_file 1) $tmpdir/boot
	mount $(getpartname $disk_file 2) $tmpdir/root
	deploy_boot $yocto_build_dir $machine $tmpdir/boot
	deploy_root $yocto_build_dir $machine $tmpdir/root $img_name
	# umount
	umount $tmpdir/boot
	umount $tmpdir/root
	clean_mnt $tmpdir
	# Check file systems
	fsck.vfat $(getpartname $disk_file 1)
	fsck.ext4 $(getpartname $disk_file 2)
}

main $@
