#!/bin/sh

set -e
set -u

default_branch="scarthgap"
branch="${BRANCH:-$default_branch}"
git_clone_opts=""
git_clone_opts="$git_clone_opts --depth 1"
git_clone_opts="$git_clone_opts -b $branch"

git clone $git_clone_opts git://git.yoctoproject.org/poky
