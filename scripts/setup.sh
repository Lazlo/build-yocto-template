#!/bin/sh

set -e
set -u

distro="$(lsb_release -c -s)"

# Taken from https://docs.yoctoproject.org/dunfell/ref-manual/ref-system-requirements.html#ubuntu-and-debian
pkgs_yocto="gawk wget git-core diffstat unzip texinfo gcc-multilib \
	build-essential chrpath socat cpio python3 python3-pip python3-pexpect \
	xz-utils debianutils iputils-ping python3-git python3-jinja2 libsdl1.2-dev \
	pylint xterm python3-subunit mesa-common-dev"

pkgs_yocto_debian="libegl1-mesa"

pkgs_yocto="$pkgs_yocto lz4"

pkgs_misc="ccache cpio locales"

case "$distro" in
	"bookworm")
		pkgs="$pkgs_yocto $pkgs_yocto_debian $pkgs_misc"
		;;
	"noble")
		pkgs="$pkgs_yocto $pkgs_misc"
		;;
	*)
		echo "ERROR: Unknown distro!"
		exit 1
		;;
esac

apt-get update && apt-get -q install -y $pkgs

# Make sure locales are installed and generated.
# Otherwise you will run into this message:
# "    Your system needs to support the en_US.UTF-8 locale."
grep -q -E '^en_US\.UTF-8 UTF-8$' /etc/locale.gen
rc="$?"
if [ "$rc" != "0" ]; then
	echo "Adding locale ..."
	echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && locale-gen
fi

if [ "$distro" = "noble" ]; then
	echo "NOTE: In order to use bitbake, you will have to disable Apparmor"
	echo "      feature 'kernel.apparmor_restrict_unprivileged_userns',"
	echo "      setting it to 0"
	echo "      Put that value into /etc/sysctl.d/60-apparmor-namespace.conf"
fi
